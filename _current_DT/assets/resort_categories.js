(function() {
  $(function() {
    $("#resort_category_name").slug_preview({
      slug_selector: '#resort_category_slug'
    });
    $(".froala").froala_editor();
  });

}).call(this);
