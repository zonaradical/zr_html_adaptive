(function() {
  $(function() {
    var news_slider;
    $(".index-slider").slick({
      dots: false,
      arrows: true,
      infinite: true,
      speed: 500,
      autoplay: false
    });
    $(".news-slider").slick({
      dots: false,
      arrows: true,
      infinite: false,
      speed: 500,
      autoplay: false,
      vertical: true,
      slidesToShow: 3,
      slidesToScroll: 1
    });
    news_slider = $(".news-slider");
    $(".slick-next").click(function() {
      if (!news_slider.find(".slide").last().hasClass(".slick-active")) {
        news_slider.find(".slide").removeClass("no-border");
        news_slider.find(".slick-active").first().addClass("no-border");
      }
    });
    $(".slick-prev").click(function() {
      if (!news_slider.find(".slide").first().hasClass(".slick-active")) {
        news_slider.find(".slide").removeClass("no-border");
        news_slider.find(".slick-active").first().addClass("no-border");
      }
    });
    return configure_search_elements();
  });

}).call(this);
