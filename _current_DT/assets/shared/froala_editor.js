(function() {
  $.fn.extend({
    froala_editor: function() {
      return $(this).editable({
        inlineMode: false,
        mediaManager: true,
        key: 'WlxvxhzxtB-16D-13lD3aliC8du==',
        imageUploadToS3: {
          bucket: gon.aws_hash.bucket,
          region: 's3-sa-east-1',
          keyStart: gon.aws_hash.key_start,
          callback: function(url, key) {},
          params: {
            acl: gon.aws_hash.acl,
            AWSAccessKeyId: gon.aws_hash.access_key,
            policy: gon.aws_hash.policy,
            signature: gon.aws_hash.signature
          }
        }
      });
    }
  });

}).call(this);
