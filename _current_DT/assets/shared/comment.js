(function() {
  var restoreActions, toggleAction;

  $('.comments-form textarea').textareaAutoSize();

  $('.comments-list').on('click', '.reply_to_link', function() {
    var action;
    action = $(this).data('action');
    restoreActions();
    return toggleAction($(this), action);
  });

  restoreActions = function() {
    $('.reply_to_link').data('action', 'reply');
    $('.reply_to_link').data('reply_to', '');
    $('.reply_to_link').children('.reply').show();
    $('.reply_to_link').children('.undo').hide();
    $('.reply_to_link').closest('p.title').removeClass('reply_to');
    $('#comment_reply_to').val('');
  };

  toggleAction = function(element, action) {
    element.children('.' + action).hide();
    if (action === 'reply') {
      element.closest('p.title').addClass('reply_to');
      element.children('.undo').show();
      element.data('action', 'undo');
      $('#comment_reply_to').val(element.data('reply-to'));
      $.scrollTo('form#new_comment', 'slow', {
        offset: -300
      });
    } else {
      element.children('.reply').show();
      element.data('action', 'reply');
    }
  };

}).call(this);
