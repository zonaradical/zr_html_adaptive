(function() {
  $.fn.extend({
    slug_preview: function(options) {
      var original_value;
      original_value = $(this).val();
      return $(this).focusout(function() {
        var element, slug_selector, value;
        element = $(this);
        value = element.val();
        if (original_value !== value) {
          slug_selector = options.slug_selector;
          if (value !== '') {
            return $.ajax({
              url: element.data('url'),
              data: {
                title: value
              },
              success: function(data) {
                return $(slug_selector).val(data.slug);
              }
            });
          } else {
            return $(slug_selector).val('');
          }
        }
      });
    }
  });

}).call(this);
