(function() {
  $('.tag_list input').tagit({
    allowSpaces: true,
    placeholderText: 'Separated by commas',
    tagSource: '/tags/autocomplete'
  });

}).call(this);
