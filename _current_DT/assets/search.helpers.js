(function() {
  this.update_visible_resorts = function() {
    var current_resort, input_search_resorts, resort_category_id, resort_option_list, resort_option_list_visible;
    resort_category_id = $('input#search_resort_categories').val();
    input_search_resorts = $('input#search_resorts');
    if (resort_category_id !== '') {
      resort_option_list = input_search_resorts.siblings('.option-list');
      resort_option_list.find('li').removeClass('hide');
      resort_option_list_visible = resort_option_list.find("li[data-category-id='" + resort_category_id + "']");
      resort_option_list.find('li').not(resort_option_list_visible).not('.default').addClass('hide');
      current_resort = input_search_resorts.siblings('div.select').find('span').html();
      if (resort_option_list.find("li:contains(" + current_resort + ")").hasClass('hide')) {
        $('input#search_resorts').val('');
        return $('input#search_resorts').siblings('div.select').find('span').html('Estação');
      }
    } else {
      resort_option_list = input_search_resorts.siblings('.option-list');
      return resort_option_list.find('li').removeClass('hide');
    }
  };

  this.configure_pagination_links = function() {
    return $('.pagination a').click(function() {
      $.get(this.href, null, (function() {
        return $.scrollTo('.search-heading', 250);
      }), 'script');
      return false;
    });
  };

  this.configure_search_elements = function() {
    $('.select').click(function() {
      var option_list, select_wrap;
      select_wrap = $(this).closest('.select-wrap');
      option_list = select_wrap.find('.option-list');
      if (option_list.is(':visible')) {
        option_list.slideUp('fast');
        $(this).find('.arrow').removeClass('active');
      } else {
        if ($('.select-wrap .option-list:visible').length) {
          $('.select-wrap .option-list:visible').hide();
          $('.select-wrap .arrow').removeClass('active');
        }
        option_list.slideDown('fast');
        $(this).find('.arrow').addClass('active');
      }
    });
    $('.option-list li').click(function() {
      var option, title;
      $(this).closest('.select-wrap').find('span').removeClass('selected');
      if ($(this).data('selected')) {
        $(this).closest('.select-wrap').find('span').addClass('selected');
      }
      title = $(this).closest('.select-wrap').find('.select .name');
      option = $(this).html();
      $(this).closest('.select-wrap').find('input[type=hidden]').val($(this).attr('data-value'));
      title.empty();
      title.html(option);
      $(this).closest('.option-list').slideUp(300);
      $(this).closest('.select-wrap').find('.arrow').removeClass('active');
      if ($(this).closest('.select-wrap').find('input#search_resort_categories').length !== 0) {
        update_visible_resorts();
      }
    });
    $(document).click(function(e) {
      if ($('.select-wrap .option-list:visible').length && !$(e.target).closest('.select-wrap').length) {
        $('.select-wrap .option-list').slideUp(300);
        $('.select-wrap .arrow').removeClass('active');
      }
    });
    $(document).keyup(function(e) {
      if (e.keyCode === 27) {
        $('.select-wrap .option-list').slideUp(300);
      }
    });
    $('.select-wrap').each(function() {
      var value;
      value = $(this).find('input[type=hidden]').val();
      if (value !== "") {
        $(this).find(".option-list li[data-value='" + value + "']").click();
        $(this).find(".option-list li[data-value='" + value + "']").data('selected', true);
        $(this).find("span").addClass('selected');
      }
    });
    if ($('#search_show_passed').is(':checked')) {
      $('label[for=show_passed]').addClass('selected');
    }
    return configure_pagination_links();
  };

}).call(this);
