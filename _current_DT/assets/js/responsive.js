$(document).ready(function () {

	$('.xs-head--tools a.i1').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$(this).closest('.xs-head').toggleClass('search');

		if ($(this).closest('.xs-head').hasClass('search'))
			$(this).closest('.xs-head').find('.xs-head--input input').focus();

		return false;
	});

	$('.xs-menu--links > ul > li > span').on('click', function (e) {
		var _ul  = $(this).closest('li').find('ul'),
			_uls = $(this).closest('ul').find('li ul');

		if (_ul.is(':visible')) {
			_ul.slideUp(300);
		} else {
			_uls.slideUp(300);
			_ul.slideDown(300);
		}
	});

	$('.xs-head--tools a.i2').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$('.xs-menu').fadeIn(300);

		return false;
	});

	$('.xs-menu--close').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$('.xs-menu').fadeOut(300);

		return false;
	});

	$('.xs-menu--login').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$('.xs-menu').fadeOut(300);
		$('.xs-login').fadeIn(300);

		return false;
	});

	$('.xs-login--close').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$('.xs-login').fadeOut(300);

		return false;
	});

	$(window).on('load resize', function () {
		var _h = Math.max($('.sm-left p.tex').outerHeight(), $('.sm-right p.tex').outerHeight());

		_h = Math.max(_h, 66);

		if (($(this).width() >= 768) & ($(this).width() < 1024)) {
			$('.xs-double--item p.tex').css('height', _h);
		} else {
			$('.xs-double--item p.tex').css('height', 'auto');
		}

		_h = Math.max($('.news-slider .news-item a.body').outerHeight(), $('ul.item-list .details p').outerHeight());
		_h = Math.max(_h, 60);

		if (($(this).width() >= 768) & ($(this).width() < 1024)) {
			$('.news-slider .news-item a.body').css('height', _h);
			$('ul.item-list .details p').css('height', _h);
		} else {
			$('.news-slider .news-item a.body').css('height', 'auto');
			$('ul.item-list .details p').css('height', 'auto');
		}

		if (($(this).width() < 1024)) {
			$('.index-slider').css('height', $('.index-slider').width() / 1.7);
		} else {
			$('.index-slider').css('height', 'auto');
		}
	});

});