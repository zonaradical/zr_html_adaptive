(function() {
  $(document).ready(function() {
    $('#_tag_list').autocomplete({
      minLength: 2,
      source: '/tags/autocomplete'
    });
    $("a.to-top").click(function() {
      $("html, body").animate({
        scrollTop: 0
      }, "slow");
    });
    $("a[href=#]").click(function(e) {
      e.preventDefault();
    });
    $("#signIn").dialog({
      autoOpen: false,
      modal: true
    });
    $("span.signIn-dialog").click(function() {
      $("#signIn").dialog("open");
      $('#new_comment').data('submitted', false);
    });
    $('#new_comment').on('submit', function() {
      if (!$(this).data('logged-user')) {
        $(this).data('submitted', true);
        $("#signIn").dialog("open");
        return false;
      }
    });
    $("form#sign_in_user, form#sign_up_user").bind("ajax:success", function(event, xhr, settings) {
      if ($('#new_comment').data('submitted')) {
        $('input[name=authenticity_token]').val(xhr.auth_token);
        $('#new_comment').data('logged-user', true).submit();
      } else {
        window.location.reload();
      }
      return $("#signIn").dialog("close");
    }).bind("ajax:error", function(event, xhr, settings, exceptions) {
      var error_messages;
      error_messages = xhr.responseJSON['error'] ? "<div class='alert alert-danger pull-left'>" + xhr.responseJSON['error'] + "</div>" : xhr.responseJSON['errors'] ? $.map(xhr.responseJSON["errors"], function(v, k) {
        return "<div class='alert alert-danger pull-left'>" + k + " " + v + "</div>";
      }).join("") : "<div class='alert alert-danger pull-left'>Unknown error</div>";
      return $(this).parents('#signIn').children('.modal-footer').html(error_messages);
    });
  });

  $(".flash").fadeIn(function() {
    var _that, secondsToFade;
    secondsToFade = $(this).data('seconds-to-fade') || 5;
    _that = this;
    setTimeout((function() {
      $(_that).fadeOut();
    }), secondsToFade * 1000);
  });

  (function(d, s, id) {
    var fjs, js;
    js = void 0;
    fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
      return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = '//connect.facebook.net/en_US/all.js#xfbml=1';
    fjs.parentNode.insertBefore(js, fjs);
  })(document, 'script', 'facebook-jssdk');

  (function() {
    var s, x;
    s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = ('https:' === document.location.protocol ? 'https://s' : 'http://i') + '.po.st/static/v4/post-widget.js#publisherKey=g9v1ifbt8labgq1ss07n';
    x = document.getElementsByTagName('script')[0];
    x.parentNode.insertBefore(s, x);
  })();

}).call(this);
