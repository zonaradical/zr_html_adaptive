(function() {
  $(function() {
    var converter, editor, initialSlide;
    $(".froala").froala_editor();
    $("#tabs").tabs();
    initialSlide = parseInt(window.location.hash.substr(1)) || 0;
    $(".tip-slider").slick({
      dots: false,
      arrows: true,
      infinite: true,
      speed: 500,
      autoplay: false,
      initialSlide: initialSlide
    });
    $('.tip-slider').on('afterChange', function(event, slick, currentSlide) {
      return window.location.hash = currentSlide;
    });
    $("#tip_title").slug_preview({
      slug_selector: '#tip_slug'
    });
    converter = Markdown.getSanitizingConverter();
    editor = new Markdown.Editor(converter);
    editor.run();
  });

}).call(this);
