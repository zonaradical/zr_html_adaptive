(function() {
  $(function() {
    $(".froala").froala_editor();
    $('.index-slider').slick({
      dots: false,
      arrows: true,
      infinite: true,
      speed: 500,
      autoplay: false
    });
    $("#resort_name").slug_preview({
      slug_selector: '#resort_slug'
    });
  });

}).call(this);
