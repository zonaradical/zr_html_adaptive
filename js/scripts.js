$(document).ready(function () {

	$('.head--tools a.i1').on('click', function (e) {
		e.preventDefault();

		$(this).closest('.head').toggleClass('search');

		if ($(this).closest('.head').hasClass('search'))
			$(this).closest('.head').find('.head--input input').focus();
	});

	$('.menu--links > ul > li > span').on('click', function (e) {
		var _ul  = $(this).closest('li').find('ul'),
			_uls = $(this).closest('ul').find('li ul');

		if (_ul.is(':visible')) {
			_ul.slideUp(300);
		} else {
			_uls.slideUp(300);
			_ul.slideDown(300);
		}
	});

	$('.head--tools a.i2').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$('.menu').fadeIn(300);

		return false;
	});

	$('.menu--close').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$('.menu').fadeOut(300);

		return false;
	});

	$('.menu--login').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$('.menu').fadeOut(300);
		$('.login').fadeIn(300);

		return false;
	});

	$('.login--close').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();

		$('.login').fadeOut(300);

		return false;
	});

	$('.intro--slider').slick({
		arrows: true,
		dots: true
	});

	$('.article--slider').slick({
		arrows: true,
		dots: true
	});

	$('.related--slider').slick({
		arrows: false,
		dots: false,
		infinite: false,
		variableWidth: true
	});

	$('.select').each(function() {
		var self = $(this);
		self.find('.select_hidden').val(self.find('.select_list li:first').attr('data-value'));
	});

	$('.select_in').on('click', function() {
		var self = $(this),
			select = self.closest('.select'),
			option_list = select.find('.select_list');

		if (option_list.is(':visible')) {
			option_list.slideUp(200);
			select.removeClass('is-opened');
			self.find('.select_arrow').removeClass('is-active');
		} else {
			if ($('.select .select_list:visible').length) {
				$('.select .select_list:visible').hide();
				$('.select .select_arrow').removeClass('is-active');
			}

			option_list.slideDown(200);
			select.addClass('is-opened');
			self.find('.arrow').addClass('is-active');
		}
	});

	$('.select_list li').on('click', function() {
		var self =  $(this),
			title = self.closest('.select').find('.select_in .select_title'),
			option = self.html();

		title.html(option);
		self.closest('.select').find('input[type=hidden]').val(self.attr('data-value'));
		self.closest('.select_list').find('li').removeClass('is-active');
		self.addClass('is-active');
		self.closest('.select_list').slideUp(200);
		self.closest('.select').removeClass('is-opened');
		self.closest('.select').find('.select_arrow').removeClass('is-active');
	});

	$(document).on('click', function(e) {
		if ($('.select .select_list:visible').length && !$(e.target).closest('.select').length) {
			$('.select').removeClass('is-opened');
			$('.select .select_list').slideUp(200);
			$('.select .select_arrow').removeClass('is-active');
		}
	});

	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			$('.select').removeClass('is-opened');
			$('.select .select_list').slideUp(200);
		}
	});

	$('.articles--filter-list > li > a').on({
		click: function (e) {
				e.preventDefault ();

				$(this).closest('li').toggleClass('active');
			}
	});

	$('.articles--filter-list > li ul li.sub').on({
		click: function () {
				$(this).toggleClass('active');
			}
	});

	$('.articles--filter-btn').on({
		click: function () {
				$(this).closest('.articles--filter').find('.articles--filter-box').fadeToggle(200);
			}
	});

	$('.tabs--items a').on({
		click: function (e) {
				e.preventDefault ();

				var _id = $(this).data('id');

				$(this).addClass('active').parent().find('.active').not(this).removeClass('active');
				$('.tabs--box[data-id=' + _id + ']').addClass('shown').parent().find('.tabs--box.shown').not('.tabs--box[data-id=' + _id + ']').removeClass('shown');
			}
	});

});